package dam.android.alejandror.baskettoday.Activities.ui.profile;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Activities.MapsActivity;
import dam.android.alejandror.baskettoday.Activities.Activities.MyAdapter;
import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.Activities.Modelos.Partido;
import dam.android.alejandror.baskettoday.Activities.ui.home.HomeFragment;
import dam.android.alejandror.baskettoday.R;

public class ProfileFragment extends Fragment implements MyAdapter.OnItemClickListener, MyAdapter.OnLongItemClickListener {

    private ProfileViewModel mViewModel;
    private View root;
    private ArrayList<Partido> myDataset;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    public final static String URL_API = "https://www.balldontlie.io/api/v1/";


    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.profile_fragment, container, false);

        new obtenerResultados().execute();
        SharedPreferences prefs = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefs.edit();

        FloatingActionButton fab = root.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirMapa(v);
            }
        });

        TextView equipo = root.findViewById(R.id.tvPrueba);
        TextView jugador = root.findViewById(R.id.tvUsuario);
        Button btCerrar = root.findViewById(R.id.btCerrarsesion);

        jugador.setText(getResources().getString(R.string.bienvenida) + " " + prefs.getString("pref_sesion", ""));
        equipo.setText(prefs.getString("pref_nombreTeam", ""));

        btCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefEditor.putString("pref_sesion", "");
                prefEditor.commit();

                prefEditor.putString("pref_pass", "");
                prefEditor.commit();

                prefEditor.putString("pref_team", "");
                prefEditor.commit();

                Fragment fragment = new HomeFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.nav_host_fragment, fragment);
                ft.commit();

                Toast.makeText(getContext(), getResources().getString(R.string.mensajeCerrar), Toast.LENGTH_SHORT).show();
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

    }

    public void abrirMapa(View view) {

        Intent intent = new Intent(getContext(), MapsActivity.class);
        startActivity(intent);

    }

    @Override
    public void onItemClick(Partido item) {

    }

    @Override
    public boolean onLongItemClick(Partido item) {
        return false;
    }

    public class obtenerResultados extends AsyncTask<Void, Void, Void> {

        EditText campoFecha;
        String fecha = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myDataset = new ArrayList<>();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;
            HttpURLConnection urlConnection2 = null;
            int total = 0;
            SharedPreferences prefs = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
            try {

                String formato = "yyyy-MM-dd";
                DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
                LocalDateTime ahora = LocalDateTime.now();
                fecha = formateador.format(ahora);

                URL url2 = new URL(URL_API + "games?per_page=7&team_ids[]=" + prefs.getString("pref_team", "") + "&seasons[]=2019&start_date=2019-01-01&end_date= " + fecha);
                urlConnection2 = (HttpURLConnection) url2.openConnection();

                urlConnection2.setRequestProperty("Connection", "close");
                urlConnection2.setConnectTimeout(15000);
                urlConnection2.setReadTimeout(10000);

                if (urlConnection2.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String resultStream2 = readStream(urlConnection2.getInputStream());

                    JSONObject json2 = new JSONObject(resultStream2);
                    JSONObject meta2 = json2.getJSONObject("meta");

                    total = meta2.getInt("total_pages");

                    System.out.println(total);
                    URL url = new URL(URL_API + "games?per_page=7&team_ids[]=" + prefs.getString("pref_team", "") + "&seasons[]=2019&start_date=2019-01-01&end_date= " + fecha + "&page=" + total);
                    urlConnection = (HttpURLConnection) url.openConnection();

                    urlConnection.setRequestProperty("Connection", "close");
                    urlConnection.setConnectTimeout(15000);
                    urlConnection.setReadTimeout(10000);


                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        String resultStream = readStream(urlConnection.getInputStream());

                        JSONObject json = new JSONObject(resultStream);
                        JSONArray jArray = json.getJSONArray("data");
                        JSONObject meta = json.getJSONObject("meta");

                        if (jArray.length() > 0) {

                            for (int i =  0; i < jArray.length(); i++) {

                                System.out.println(i);
                                JSONObject objectSuperior = jArray.getJSONObject(i);
                                JSONObject objectLocal = objectSuperior.getJSONObject("home_team");
                                JSONObject objectVisitante = objectSuperior.getJSONObject("visitor_team");

                                Equipo equipoLocal = new Equipo();
                                equipoLocal.setCiudad(objectLocal.getString("city"));
                                equipoLocal.setConferencia(objectLocal.getString("conference"));
                                equipoLocal.setDivision(objectLocal.getString("division"));
                                equipoLocal.setNombre(objectLocal.getString("full_name"));

                                Equipo equipoVisitante = new Equipo();
                                equipoVisitante.setCiudad(objectVisitante.getString("city"));
                                equipoVisitante.setConferencia(objectVisitante.getString("conference"));
                                equipoVisitante.setDivision(objectVisitante.getString("division"));
                                equipoVisitante.setNombre(objectVisitante.getString("full_name"));

                                Partido partido = new Partido();
                                partido.setEquipoLocal(equipoLocal);
                                partido.setEquipoVisitante(equipoVisitante);
                                partido.setFecha(objectSuperior.getString("date").substring(0, 10));
                                partido.setMarcadorLocal(objectSuperior.getInt("home_team_score"));
                                partido.setMarcadorVisitante(objectSuperior.getInt("visitor_team_score"));

                                myDataset.add(partido);
                            }
                        }
                    }
                } else {
                }
            } catch (IOException e) {

                Log.i("IOException", e.getMessage());

            } catch (JSONException e) {

                Log.i("JSONException", e.getMessage());

            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (myDataset.size() > 0) {
                RecyclerView rv = root.findViewById(R.id.recyclerEquipoPerfil);
                mAdapter = new MyAdapter(myDataset, ProfileFragment.this::onItemClick, ProfileFragment.this::onLongItemClick);
                //rv.setHasFixedSize(true);
                rv.setAdapter(mAdapter);
                layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                rv.setLayoutManager(layoutManager);
            } else {

            }

        }
    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }


}
