package dam.android.alejandror.baskettoday.Activities.Modelos;

public class Estadistica {

    public Jugador jugador;
    public Equipo equipoLocal;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Equipo getEquipoEstadistica() {
        return equipoLocal;
    }

    public void setEquipoEstadistica(Equipo equipo) {
        this.equipoLocal = equipo;
    }
}
