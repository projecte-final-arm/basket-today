package dam.android.alejandror.baskettoday.Activities.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.R;

public class RegistroActivity extends AppCompatActivity {

    public final static String URL_API = "https://www.balldontlie.io/api/v1/";
    private ArrayList<Equipo> myDataset;
    private ArrayList<String> equiposString;
    private ArrayList<Integer> equiposInt;

    private EditText userET;
    private EditText passET;
    private Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        myDataset = new ArrayList<>();
        equiposString = new ArrayList<>();
        equiposInt = new ArrayList<>();
        userET = findViewById(R.id.etUsuario);
        passET = findViewById(R.id.etPassword);
        spinner = findViewById(R.id.teamSpinner);
        Button registro =  findViewById(R.id.btRegistrarse);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ejecutarServicio("http://192.168.1.133:80/baskettoday/registrar.php");
            }
        });

        new obtenerEquipos().execute();

    }

    private void ejecutarServicio(String URL) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("<br />")) {
                    Toast.makeText(RegistroActivity.this, "Usuario existente", Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametres = new HashMap<>();
                System.out.println(equiposInt.get(spinner.getSelectedItemPosition()).toString());
                parametres.put("contra", passET.getText().toString());
                parametres.put("usuario", userET.getText().toString());
                parametres.put("equipo", equiposInt.get(spinner.getSelectedItemPosition()).toString());
                return parametres;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public class obtenerEquipos extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("WrongThread")
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;

            try {

                URL url = new URL(URL_API + "teams");
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(15000);
                urlConnection.setReadTimeout(10000);

                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String resultStream = readStream(urlConnection.getInputStream());

                    JSONObject json = new JSONObject(resultStream);
                    JSONArray jArray = json.getJSONArray("data");

                    if (jArray.length() > 0) {

                        for (int i = 0; i < jArray.length(); i++) {

                            JSONObject objectSuperior = jArray.getJSONObject(i);

                            Equipo equipo = new Equipo();
                            equipo.setId(objectSuperior.getInt("id"));
                            equipo.setCiudad(objectSuperior.getString("city"));
                            equipo.setConferencia(objectSuperior.getString("conference"));
                            equipo.setDivision(objectSuperior.getString("division"));
                            equipo.setNombre(objectSuperior.getString("full_name"));

                            myDataset.add(equipo);
                            equiposString.add(equipo.getNombre());
                            equiposInt.add(equipo.getId());
                        }


                    }
                } else {
                    Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {

                Log.i("IOException", e.getMessage());

            } catch (JSONException e) {

                Log.i("JSONException", e.getMessage());

            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Spinner s = findViewById(R.id.teamSpinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    R.layout.spinner_item, equiposString);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            s.setAdapter(adapter);


        }
    }


    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

}
