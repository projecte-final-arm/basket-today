package dam.android.alejandror.baskettoday.Activities.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import dam.android.alejandror.baskettoday.R;

public class SplashScreenActivity extends AppCompatActivity {

    private int SPLASH_TIME_OUT = 3000;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        context = this;
        Handler handler = new Handler();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashScreenActivity.this.startActivity(new Intent(SplashScreenActivity.this.getApplicationContext(), PestanyasActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
