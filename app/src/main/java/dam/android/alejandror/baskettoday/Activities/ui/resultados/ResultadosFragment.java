package dam.android.alejandror.baskettoday.Activities.ui.resultados;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Activities.InformacionPartido;
import dam.android.alejandror.baskettoday.Activities.Activities.MyAdapter;
import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.Activities.Modelos.Partido;
import dam.android.alejandror.baskettoday.Activities.Resources.DatePickerFragment;
import dam.android.alejandror.baskettoday.R;


public class ResultadosFragment extends Fragment implements MyAdapter.OnItemClickListener, MyAdapter.OnLongItemClickListener, View.OnClickListener {

    private NotificationsViewModel notificationsViewModel;
    private ArrayList<Partido> myDataset;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView rv;
    public RecyclerView listView;

    public final static String URL_API = "https://www.balldontlie.io/api/v1/";
    private View root;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        root = inflater.inflate(R.layout.fragment_resultados, container, false);

        FloatingActionButton fab = root.findViewById(R.id.fabResultados);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        // +1 because January is zero
                        final String selectedDate = year + "-" + (month + 1) + "-" + day;
                        Toast.makeText(getContext(), selectedDate, Toast.LENGTH_SHORT).show();

                        myDataset.clear();
                        new obtenerResultados(selectedDate).execute();
                    }
                });

                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        setUi();

        return root;
    }

    public void setUi() {

        myDataset = new ArrayList<>();

        new obtenerResultados("").execute();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                RecyclerView rv = root.findViewById(R.id.recyclerView);
                mAdapter = new MyAdapter(myDataset, ResultadosFragment.this::onItemClick, ResultadosFragment.this::onLongItemClick);
                //rv.setHasFixedSize(true);
                rv.setAdapter(mAdapter);
                layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                rv.setLayoutManager(layoutManager);

            }
        }, 2000);


    }

    @Override
    public void onItemClick(Partido item) {

        SharedPreferences prefs = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefs.edit();

        prefEditor.putInt("pref_idpartido",item.getId());
        prefEditor.commit();

        startActivity(new Intent(getContext(), InformacionPartido.class));

    }


    @Override
    public boolean onLongItemClick(Partido item) {
        return false;
    }

    @Override
    public void onClick(View v) {
    }

    public class obtenerResultados extends AsyncTask<Void, Void, Void> {

        EditText campoFecha;
        String fecha = "";
        TextView tError = root.findViewById(R.id.textError);


        public obtenerResultados(String selectedDate) {

            fecha = selectedDate;
            tError.setVisibility(View.INVISIBLE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;

            try {

                if (fecha.equals("")) {

                    String formato = "yyyy-MM-dd";
                    DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
                    LocalDateTime ahora = LocalDateTime.now();
                    fecha = formateador.format(ahora);
                }

                URL url = new URL(URL_API + "games?dates[]=" + fecha);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(15000);
                urlConnection.setReadTimeout(10000);


                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String resultStream = readStream(urlConnection.getInputStream());

                    JSONObject json = new JSONObject(resultStream);
                    JSONArray jArray = json.getJSONArray("data");

                    if (jArray.length() > 0) {

                        for (int i = 0; i < jArray.length(); i++) {

                            JSONObject objectSuperior = jArray.getJSONObject(i);
                            JSONObject objectLocal = objectSuperior.getJSONObject("home_team");
                            JSONObject objectVisitante = objectSuperior.getJSONObject("visitor_team");

                            Equipo equipoLocal = new Equipo();
                            equipoLocal.setCiudad(objectLocal.getString("city"));
                            equipoLocal.setConferencia(objectLocal.getString("conference"));
                            equipoLocal.setDivision(objectLocal.getString("division"));
                            equipoLocal.setNombre(objectLocal.getString("full_name"));

                            Equipo equipoVisitante = new Equipo();
                            equipoVisitante.setCiudad(objectVisitante.getString("city"));
                            equipoVisitante.setConferencia(objectVisitante.getString("conference"));
                            equipoVisitante.setDivision(objectVisitante.getString("division"));
                            equipoVisitante.setNombre(objectVisitante.getString("full_name"));

                            Partido partido = new Partido();
                            partido.setEquipoLocal(equipoLocal);
                            partido.setId(objectSuperior.getInt("id"));
                            partido.setEquipoVisitante(equipoVisitante);
                            partido.setFecha(objectSuperior.getString("date").substring(0, 10));
                            partido.setMarcadorLocal(objectSuperior.getInt("home_team_score"));
                            partido.setMarcadorVisitante(objectSuperior.getInt("visitor_team_score"));



                            myDataset.add(partido);
                        }


                    } else {


                    }

                } else {
                    Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {

                Log.i("IOException", e.getMessage());

            } catch (JSONException e) {

                Log.i("JSONException", e.getMessage());

            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (myDataset.size() > 0) {
                RecyclerView rv = root.findViewById(R.id.recyclerView);
                mAdapter = new MyAdapter(myDataset, ResultadosFragment.this::onItemClick, ResultadosFragment.this::onLongItemClick);
                //rv.setHasFixedSize(true);
                rv.setAdapter(mAdapter);
                layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                rv.setLayoutManager(layoutManager);
            } else {
                tError.setVisibility(View.VISIBLE);

            }

        }
    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }
}