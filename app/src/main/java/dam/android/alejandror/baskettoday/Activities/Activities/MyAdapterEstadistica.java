package dam.android.alejandror.baskettoday.Activities.Activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Modelos.Estadistica;
import dam.android.alejandror.baskettoday.R;

public class MyAdapterEstadistica extends RecyclerView.Adapter<MyAdapterEstadistica.MyViewHolder> {

    private ArrayList<Estadistica> myDataSet;


    public interface OnItemClickListener {
        void onItemClick(Estadistica item);
    }

    public interface OnLongItemClickListener {
        boolean onLongItemClick(Estadistica item);
    }

    private OnItemClickListener listener;
    private OnLongItemClickListener longListener;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRebotes;
        TextView tvPuntos;
        TextView tvAsistencias;
        TextView tvTapones;
        TextView tvNombre;
        TextView tvEquipo;
        CardView card;

        public MyViewHolder(View view) {

            super(view);
            this.tvRebotes = view.findViewById(R.id.rebotesPartido);
            this.tvPuntos = view.findViewById(R.id.puntosPartido);
            this.tvAsistencias = view.findViewById(R.id.asistenciasPartido);
            this.tvTapones = view.findViewById(R.id.taponesPartido);
            this.tvNombre = view.findViewById(R.id.tvNombreJugador);
            this.tvEquipo = view.findViewById(R.id.tvPrueba);
            this.card = view.findViewById(R.id.cardEstadisticas);
        }

        public void bind(Estadistica data, OnItemClickListener listener, OnLongItemClickListener longListener) {

            this.tvRebotes.setText(data.getJugador().getRebotes() + "");
            this.tvPuntos.setText(data.getJugador().getPuntos() + "");
            this.tvAsistencias.setText(data.getJugador().getAsistencias() + "");
            this.tvTapones.setText(data.getJugador().getTapones() + "");
            this.tvNombre.setText(data.getJugador().getNombre() + " " + data.getJugador().getApellidos());
            this.tvEquipo.setText(data.getEquipoEstadistica().getNombre());

        }

    }


    public MyAdapterEstadistica(ArrayList<Estadistica> myDataSet, OnItemClickListener listener, OnLongItemClickListener longListener) {
        this.listener = listener;
        this.longListener = longListener;
        this.myDataSet = myDataSet;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_estadistica, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataSet.get(position), listener, longListener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}
