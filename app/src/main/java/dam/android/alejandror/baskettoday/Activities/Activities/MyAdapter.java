package dam.android.alejandror.baskettoday.Activities.Activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Modelos.Partido;
import dam.android.alejandror.baskettoday.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    //    private final TodoListDBManager todoListDBManager;
    private ArrayList<Partido> myDataSet;


    public interface OnItemClickListener {
        void onItemClick(Partido item);
    }

    public interface OnLongItemClickListener {
        boolean onLongItemClick(Partido item);
    }

    private OnItemClickListener listener;
    private OnLongItemClickListener longListener;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvMarLocal;
        TextView tvMarVisitante;
        TextView tvNomLocal;
        TextView tvNomVisitante;
        TextView tvFecha;
        CardView card;

        public MyViewHolder(View view) {

            super(view);
            this.tvMarLocal = view.findViewById(R.id.tvMarcadorLocal);
            this.tvMarVisitante = view.findViewById(R.id.tvMarcadorVisitante);
            this.tvNomLocal = view.findViewById(R.id.tvEquipoLocal);
            this.tvNomVisitante = view.findViewById(R.id.tvEquipoVisitante);
            this.tvFecha = view.findViewById(R.id.tvFecha);
            this.card = view.findViewById(R.id.card);
        }

        public void bind(Partido data, OnItemClickListener listener, OnLongItemClickListener longListener) {

            this.tvNomLocal.setText(data.getEquipoLocal().getNombre());
            this.tvNomVisitante.setText(data.getEquipoVisitante().getNombre());
            this.tvMarLocal.setText(data.getMarcadorLocal() + "");
            this.tvMarVisitante.setText(data.getMarcadorVisitante() + "");
            this.tvFecha.setText(data.getFecha() + "");
            this.card.setOnClickListener(v -> listener.onItemClick(data));
            this.card.setOnLongClickListener(v -> longListener.onLongItemClick(data));
        }

    }

    public MyAdapter(ArrayList<Partido> myDataSet, OnItemClickListener listener, OnLongItemClickListener longListener) {
        this.listener = listener;
        this.longListener = longListener;
        this.myDataSet = myDataSet;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_layout, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataSet.get(position), listener, longListener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}
