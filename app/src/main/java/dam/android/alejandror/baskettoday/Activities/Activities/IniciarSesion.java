package dam.android.alejandror.baskettoday.Activities.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.R;

public class IniciarSesion extends AppCompatActivity {

    String usuario = "";
    String password = "";

    EditText etUser;
    EditText etPass;

    public final static String URL_API = "https://www.balldontlie.io/api/v1/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);

        Button registrarse = findViewById(R.id.btRegistrarse);
        Button iniciarSesion = findViewById(R.id.btEntrar);

        etUser = findViewById(R.id.etUsuario);
        etPass = findViewById(R.id.etPassword);


        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), RegistroActivity.class));

            }
        });

        iniciarSesion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!(etPass.getText().toString().equals("") && etUser.getText().toString().equals(""))) {
                    iniciarSesion("http://192.168.1.133:80/baskettoday/iniciarSesion.php?usuario=" + etUser.getText().toString() + "&contra=" + etPass.getText().toString());
                } else {
                    Toast.makeText(IniciarSesion.this, "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, PestanyasActivity.class));
    }

    private void iniciarSesion(String URL) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;
                try {

                    if (response.length() > 0) {
                        jsonObject = response.getJSONObject(0);

                        SharedPreferences prefs = getSharedPreferences("preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor prefEditor = prefs.edit();

                        prefEditor.putString("pref_sesion", jsonObject.getString("usuario"));
                        prefEditor.commit();

                        prefEditor.putString("pref_pass", jsonObject.getString("contraseña"));
                        prefEditor.commit();

                        prefEditor.putString("pref_team", jsonObject.getString("equipoFavorito"));
                        prefEditor.commit();

                        new obtenerEquipos().execute();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), PestanyasActivity.class));
                                finish();
                            }
                        }, 1000);

                    } else {
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(IniciarSesion.this, "Usuario inexistente", Toast.LENGTH_SHORT).show();
                SharedPreferences prefs = getSharedPreferences("preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = prefs.edit();

                prefEditor.putString("pref_sesion", "");
                prefEditor.commit();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

    }

    public class obtenerEquipos extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("WrongThread")
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;

            SharedPreferences prefs = getSharedPreferences("preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = prefs.edit();

            try {

                URL url = new URL(URL_API + "teams/" + prefs.getString("pref_team", ""));
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(15000);
                urlConnection.setReadTimeout(10000);

                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String resultStream = readStream(urlConnection.getInputStream());

                    JSONObject json = new JSONObject(resultStream);

                    Equipo equipo = new Equipo();
                    equipo.setId(json.getInt("id"));
                    equipo.setCiudad(json.getString("city"));
                    equipo.setConferencia(json.getString("conference"));
                    equipo.setDivision(json.getString("division"));
                    equipo.setNombre(json.getString("full_name"));

                    prefEditor.putString("pref_nombreTeam", equipo.getNombre());
                    prefEditor.commit();


                } else {
                    Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {

                Log.i("IOException", e.getMessage());

            } catch (JSONException e) {

                Log.i("JSONException", e.getMessage());

            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

}
