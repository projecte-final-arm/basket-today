package dam.android.alejandror.baskettoday.Activities.Modelos;

public class Estadio {

    public String direccion;
    public String nombre;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
