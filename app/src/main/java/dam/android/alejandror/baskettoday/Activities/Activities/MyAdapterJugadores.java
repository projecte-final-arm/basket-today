package dam.android.alejandror.baskettoday.Activities.Activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Modelos.Jugador;
import dam.android.alejandror.baskettoday.R;

public class MyAdapterJugadores extends RecyclerView.Adapter<MyAdapterJugadores.MyViewHolder> {

    //    private final TodoListDBManager todoListDBManager;
    private ArrayList<Jugador> myDataSet;


    public interface OnItemClickListener {
        void onItemClick(Jugador item);
    }

    public interface OnLongItemClickListener {
        boolean onLongItemClick(Jugador item);
    }

    private OnItemClickListener listener;
    private OnLongItemClickListener longListener;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRebotes;
        TextView tvPuntos;
        TextView tvAsistencias;
        TextView tvTapones;
        TextView tvNombre;
        TextView tvEquipo;
        CardView card;

        public MyViewHolder(View view) {

            super(view);
            this.tvRebotes = view.findViewById(R.id.rebotesPartido);
            this.tvPuntos = view.findViewById(R.id.puntosPartido);
            this.tvAsistencias = view.findViewById(R.id.asistenciasPartido);
            this.tvTapones = view.findViewById(R.id.taponesPartido);
            this.tvNombre = view.findViewById(R.id.tvNombreJugador);
            this.tvEquipo = view.findViewById(R.id.tvPrueba);
            this.card = view.findViewById(R.id.cardJugadores);
        }

        public void bind(Jugador data, OnItemClickListener listener, OnLongItemClickListener longListener) {

            this.tvRebotes.setText(data.getRebotes() + "");
            this.tvPuntos.setText(data.getPuntos() + "");
            this.tvAsistencias.setText(data.getAsistencias() + "");
            this.tvTapones.setText(data.getTapones() + "");
            this.tvNombre.setText(data.getNombre() + " " + data.getApellidos());
            this.tvEquipo.setText(data.getEquipo());

            this.card.setOnClickListener(v -> listener.onItemClick(data));
            this.card.setOnLongClickListener(v -> longListener.onLongItemClick(data));
        }

    }

    public MyAdapterJugadores(ArrayList<Jugador> myDataSet, OnItemClickListener listener, OnLongItemClickListener longListener) {
        this.listener = listener;
        this.longListener = longListener;
        this.myDataSet = myDataSet;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_layout_jugadores, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataSet.get(position), listener, longListener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}
