package dam.android.alejandror.baskettoday.Activities.ui.estadistiques;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Activities.MyAdapterJugadores;
import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.Activities.Modelos.Jugador;
import dam.android.alejandror.baskettoday.R;

public class DashboardFragment extends Fragment implements MyAdapterJugadores.OnItemClickListener, MyAdapterJugadores.OnLongItemClickListener {

    private DashboardViewModel dashboardViewModel;
    public final static String URL_API = "https://www.balldontlie.io/api/v1/";
    private ArrayList<Equipo> myDataset;
    private ArrayList<Jugador> myDatasetPlayers;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> años;

    private View root;
    public Dialog dialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        root = inflater.inflate(R.layout.fragment_stats, container, false);
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.floating_menu);

        Button botonFiltrar = root.findViewById(R.id.botonFiltrar);
        EditText editNombre = root.findViewById(R.id.editNombre);
        Spinner spinner = root.findViewById(R.id.spinnerAnyos);
        botonFiltrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editNombre.getText().toString().equals("")) {

                    String año = años.get(spinner.getSelectedItemPosition());
                    System.out.println(año + "este es el año");
                    new obtenerJugadores(editNombre.getText().toString(), año).execute();

                }
            }
        });
        años = new ArrayList<>();
        añadirAños();


        return root;
    }


    public void añadirAños() {

        for (int i = 2019; i >= 2000; i--) {
            años.add(String.valueOf(i));
        }

        Spinner s = root.findViewById(R.id.spinnerAnyos);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.spinner_item2, años);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);

    }

    @Override
    public void onItemClick(Jugador item) {

    }

    @Override
    public boolean onLongItemClick(Jugador item) {
        return false;
    }

    public class obtenerJugadores extends AsyncTask<Void, Void, Void> {

        String nombreString = "";
        String año= "";
        boolean continuar = true;


        private obtenerJugadores(String nombreString, String año) {

            this.nombreString = nombreString;
            myDatasetPlayers = new ArrayList<>();
            this.año = año;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            HttpURLConnection urlConnection2 = null;
            HttpURLConnection urlConnection3 = null;

            try {
                int totalPaginas = 5;

                for (int i = 1; i < totalPaginas + 1 && continuar; i++) {

                    URL url2 = new URL(URL_API + "players?search=" + nombreString + "&page=" + i);
                    urlConnection2 = (HttpURLConnection) url2.openConnection();

                    urlConnection2.setRequestProperty("Connection", "close");
                    urlConnection2.setConnectTimeout(15000);
                    urlConnection2.setReadTimeout(10000);

                    if (urlConnection2.getResponseCode() == HttpURLConnection.HTTP_OK) {

                        String resultStream2 = readStream(urlConnection2.getInputStream());
                        JSONObject json2 = new JSONObject(resultStream2);
                        JSONArray jArray = json2.getJSONArray("data");

                        JSONObject meta = json2.getJSONObject("meta");
                        totalPaginas = meta.getInt("total_pages");

                        if (jArray.length() > 0) {
                            for (int j = 0; j < jArray.length(); j++) {

                                JSONObject jObject = jArray.getJSONObject(j);

                                URL url3 = new URL(URL_API + "season_averages?season=" + año + "&player_ids[]=" + jObject.getInt("id"));
                                urlConnection3 = (HttpURLConnection) url3.openConnection();

                                urlConnection3.setRequestProperty("Connection", "close");
                                urlConnection3.setConnectTimeout(15000);
                                urlConnection3.setReadTimeout(10000);

                                if (urlConnection3.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                    String resultStream3 = readStream(urlConnection3.getInputStream());
                                    JSONObject json3 = new JSONObject(resultStream3);
                                    JSONArray jArray2 = json3.getJSONArray("data");

                                    System.out.println(json3);
                                    if (jArray2.length() > 0) {

                                        JSONObject jEstadisticas = jArray2.getJSONObject(0);
                                        JSONObject jEquipo = jObject.getJSONObject("team");
                                        Jugador jugador = new Jugador();
                                        jugador.setId(jObject.getInt("id"));
                                        jugador.setNombre(jObject.getString("first_name"));
                                        jugador.setApellidos(jObject.getString("last_name"));
                                        jugador.setPosicion(jObject.getString("position"));
                                        jugador.setEquipo(jEquipo.getString("full_name"));
                                        jugador.setPuntos(jEstadisticas.getDouble("pts"));
                                        jugador.setAsistencias(jEstadisticas.getDouble("ast"));
                                        jugador.setRebotes(jEstadisticas.getDouble("reb"));
                                        jugador.setTapones(jEstadisticas.getDouble("blk"));

                                        System.out.println(jugador.getNombre());
                                        myDatasetPlayers.add(jugador);

                                    }

                                }


                            }
                        } else {

                            continuar = false;
                        }
                    }
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (JSONException ex) {
                ex.printStackTrace();
            }


            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!continuar) {
                Toast.makeText(getContext(), "No se ha encontrado ningun jugador con este nombre", Toast.LENGTH_SHORT).show();
            } else {

                RecyclerView rv = root.findViewById(R.id.recyclerJugadores);
                mAdapter = new MyAdapterJugadores(myDatasetPlayers, DashboardFragment.this::onItemClick, DashboardFragment.this::onLongItemClick);
                rv.setHasFixedSize(true);
                rv.setAdapter(mAdapter);
                layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                rv.setLayoutManager(layoutManager);
            }
        }

    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }
}