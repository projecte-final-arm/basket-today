package dam.android.alejandror.baskettoday.Activities.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dam.android.alejandror.baskettoday.Activities.Modelos.Estadio;
import dam.android.alejandror.baskettoday.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String nombreMapa;
    private LocationManager locationManager;
    private Marker marker;
    private ArrayList<Estadio> estadios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        obtenerMapas("http://192.168.1.133:80/baskettoday/estadiosNba.php");

        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        estadios = new ArrayList<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            mMap = googleMap;

            Location location = null;
            LatLng punto = null;

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses;

            for (int i = 0; i < estadios.size(); i++) {
                addresses = geocoder.getFromLocationName(estadios.get(i).getDireccion(), 1);

                if (addresses.size() > 0) {

                    double latitude = addresses.get(0).getLatitude();
                    double longitude = addresses.get(0).getLongitude();

                    punto = new LatLng(latitude, longitude);

                    marker = mMap.addMarker(new MarkerOptions().position(punto).title(estadios.get(i).getNombre()));
                }

            }
            float zoom = 2;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(punto, zoom));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        startActivity(new Intent(this, PestanyasActivity.class));
    }

    private void obtenerMapas(String URL) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //try {

                estadios = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {

                    try {

                        JSONObject jsonObject = response.getJSONObject(i);

                        Estadio estadio = new Estadio();
                        estadio.setDireccion(jsonObject.getString("direccion"));
                        estadio.setNombre(jsonObject.getString("equipo"));

                        estadios.add(estadio);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                //} catch (JSONException e) {
                //    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
               // }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

    }
}
