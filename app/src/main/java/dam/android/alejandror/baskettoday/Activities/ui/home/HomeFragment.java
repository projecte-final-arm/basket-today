package dam.android.alejandror.baskettoday.Activities.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import dam.android.alejandror.baskettoday.Activities.Activities.IniciarSesion;
import dam.android.alejandror.baskettoday.Activities.ui.profile.ProfileFragment;
import dam.android.alejandror.baskettoday.R;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private SharedPreferences preferences;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        SharedPreferences prefs = this.getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefs.edit();

        if (!prefs.getString("pref_sesion","").equals("")) {
            Fragment fragment = new ProfileFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.nav_host_fragment, fragment);
            ft.commit();
        }
        Button button = root.findViewById(R.id.buttonRegistro);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), IniciarSesion.class));
            }
        });


        return root;
    }


}