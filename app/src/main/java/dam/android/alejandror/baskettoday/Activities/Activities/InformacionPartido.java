package dam.android.alejandror.baskettoday.Activities.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.android.alejandror.baskettoday.Activities.Modelos.Equipo;
import dam.android.alejandror.baskettoday.Activities.Modelos.Estadistica;
import dam.android.alejandror.baskettoday.Activities.Modelos.Jugador;
import dam.android.alejandror.baskettoday.R;

public class InformacionPartido extends AppCompatActivity implements MyAdapterEstadistica.OnItemClickListener, MyAdapterEstadistica.OnLongItemClickListener{

    private ArrayList<Estadistica> myDataset;
    public final static String URL_API = "https://www.balldontlie.io/api/v1/";
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_partido);

        new obtenerResultados().execute();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(), PestanyasActivity.class));
    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);

            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

    @Override
    public void onItemClick(Estadistica item) {

    }

    @Override
    public boolean onLongItemClick(Estadistica item) {
        return false;
    }

    public class obtenerResultados extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myDataset = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            HttpURLConnection urlConnection = null;
            SharedPreferences prefs = getSharedPreferences("preferences", Context.MODE_PRIVATE);

            try {

                System.out.println(prefs.getInt("pref_idpartido", 0));
                URL url = new URL(URL_API + "stats?game_ids[]=" + prefs.getInt("pref_idpartido", 0));
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(15000);
                urlConnection.setReadTimeout(10000);


                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String resultStream = readStream(urlConnection.getInputStream());

                    JSONObject json = new JSONObject(resultStream);
                    JSONArray jArray = json.getJSONArray("data");

                    if (jArray.length() > 0) {

                        for (int i = 0; i < jArray.length(); i++) {

                            JSONObject objectSuperior = jArray.getJSONObject(i);
                            JSONObject equipoJSON = objectSuperior.getJSONObject("team");
                            JSONObject jugadorJSON = objectSuperior.getJSONObject("player");

                            Equipo equipoLocal = new Equipo();
                            equipoLocal.setCiudad(equipoJSON.getString("city"));
                            equipoLocal.setConferencia(equipoJSON.getString("conference"));
                            equipoLocal.setDivision(equipoJSON.getString("division"));
                            equipoLocal.setNombre(equipoJSON.getString("full_name"));

                            Jugador jugador = new Jugador();
                            jugador.setId(jugadorJSON.getInt("id"));
                            jugador.setNombre(jugadorJSON.getString("first_name"));
                            jugador.setApellidos(jugadorJSON.getString("last_name"));
                            jugador.setPosicion(jugadorJSON.getString("position"));
                            jugador.setEquipo(equipoJSON.getString("full_name"));
                            jugador.setPuntos(objectSuperior.getDouble("pts"));
                            jugador.setAsistencias(objectSuperior.getDouble("ast"));
                            jugador.setRebotes(objectSuperior.getDouble("reb"));
                            jugador.setTapones(objectSuperior.getDouble("blk"));

                            Estadistica estadistica = new Estadistica();
                            estadistica.setEquipoEstadistica(equipoLocal);
                            estadistica.setJugador(jugador);

                            myDataset.add(estadistica);
                        }

                    }

                } else {
                    Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {

                Log.i("IOException", e.getMessage());

            } catch (JSONException e) {

                Log.i("JSONException", e.getMessage());

            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (myDataset.size() > 0) {
                RecyclerView rv = findViewById(R.id.recyclerEstadisticas);
                mAdapter = new MyAdapterEstadistica(myDataset, InformacionPartido.this::onItemClick, InformacionPartido.this::onLongItemClick);
                rv.setAdapter(mAdapter);
                layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                rv.setLayoutManager(layoutManager);
            }
        }



    }

}
